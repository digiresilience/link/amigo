require('@digiresilience/eslint-config-amigo/patch/modern-module-resolution');
module.exports = {
  extends: [
    "@digiresilience/eslint-config-amigo/profile/node",
    "@digiresilience/eslint-config-amigo/profile/typescript"
  ],
  rules: {
    // TODO: enable this after jest fixes this issue https://github.com/nodejs/node/issues/38343
    "unicorn/prefer-node-protocol": "off"
  },
  parserOptions: { tsconfigRootDir: __dirname }
};

