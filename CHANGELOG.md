# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.5.11](https://gitlab.com/digiresilience/link/amigo/compare/0.5.10...0.5.11) (2021-10-19)

### [0.5.10](https://gitlab.com/digiresilience/link/amigo/compare/0.5.9...0.5.10) (2021-10-19)

### [0.5.9](https://gitlab.com/digiresilience/link/amigo/compare/0.5.8...0.5.9) (2021-10-19)

### [0.5.8](https://gitlab.com/digiresilience/link/amigo/compare/0.5.7...0.5.8) (2021-10-11)

### [0.5.7](https://gitlab.com/digiresilience/link/amigo/compare/0.5.6...0.5.7) (2021-10-11)

### [0.5.6](https://gitlab.com/digiresilience/link/amigo/compare/0.5.5...0.5.6) (2021-10-11)

### [0.5.5](https://gitlab.com/digiresilience/link/amigo/compare/0.5.4...0.5.5) (2021-10-11)

### [0.5.4](https://gitlab.com/digiresilience/link/amigo/compare/0.5.3...0.5.4) (2021-10-08)

### [0.5.3](https://gitlab.com/digiresilience/link/amigo/compare/0.5.2...0.5.3) (2021-06-22)


### Features

* do not log requests for hapi routes tagged with `nolog` ([6a682e6](https://gitlab.com/digiresilience/link/amigo/commit/6a682e6fbe316ca9092e0a6437461e2c3a1cb28a))
* do not log the /graphql route by default ([7945ab1](https://gitlab.com/digiresilience/link/amigo/commit/7945ab14ecee4dbc200627d6c2cb46b80665f3ad))

### [0.5.2](https://gitlab.com/digiresilience/link/amigo/compare/0.5.1...0.5.2) (2021-06-22)


### Features

* add optional configuration overrides ([4271186](https://gitlab.com/digiresilience/link/amigo/commit/4271186950919dffb754265199ac90b7cc760115))

### [0.5.1](https://gitlab.com/digiresilience/link/amigo/compare/0.5.0...0.5.1) (2021-05-03)


### Bug Fixes

* temporarily disable the nod protocl eslint rule ([7be11d8](https://gitlab.com/digiresilience/link/amigo/commit/7be11d8197dae610261acec06d1280dbaa5c96ed))

## [0.5.0](https://gitlab.com/digiresilience/link/amigo/compare/0.4.1...0.5.0) (2021-05-03)


### ⚠ BREAKING CHANGES

* bump deps
* bump typescript to 4.2.4

### Features

* bump @digiresilience/amigo-dev ([d409444](https://gitlab.com/digiresilience/link/amigo/commit/d4094443b10494a2c0cb8217ddb7a45ba84b80c2))
* bump deps ([fa62cbe](https://gitlab.com/digiresilience/link/amigo/commit/fa62cbe6978ccf18042344345b0ca8a22cb5e9b2))
* bump typescript to 4.2.4 ([4899a7c](https://gitlab.com/digiresilience/link/amigo/commit/4899a7c68b25e295a3f8362a8312b1d63ad427af))


### Bug Fixes

* appease the linter ([4312e4e](https://gitlab.com/digiresilience/link/amigo/commit/4312e4e748d9a98567264c5e388d4086545271b1))
* update amigo-dev ([8d9b76d](https://gitlab.com/digiresilience/link/amigo/commit/8d9b76d34cc69e2d35d4c9cc1e558e65a2d4e7a2))

### [0.4.1](https://gitlab.com/digiresilience/link/amigo/compare/0.4.0...0.4.1) (2021-04-30)


### Bug Fixes

* ensure client id is not propagated to the database ([fb92917](https://gitlab.com/digiresilience/link/amigo/commit/fb9291773b4ef6823da12a871c48c55acba33796))

## [0.4.0](https://gitlab.com/digiresilience/link/amigo/compare/0.3.4...0.4.0) (2021-04-30)


### ⚠ BREAKING CHANGES

* update next-auth to 3.19.3

### Features

* update next-auth to 3.19.3 ([580d5ec](https://gitlab.com/digiresilience/link/amigo/commit/580d5ec9509da29cb049c0fc64b45e1f1531dcde))

### [0.3.4](https://gitlab.com/digiresilience/link/amigo/compare/0.3.3...0.3.4) (2020-12-04)


### Bug Fixes

* add isActive field to User record ([7afa477](https://gitlab.com/digiresilience/link/amigo/commit/7afa4778e33d1c99d53c43fd8f0a7ff3f87774f5))
* remove redundant eslint rules after upgrading amigo-dev ([5b1c044](https://gitlab.com/digiresilience/link/amigo/commit/5b1c0444f28d8ee69a8defe9134ab7bb9807b556))
* update dependencies ([c28f941](https://gitlab.com/digiresilience/link/amigo/commit/c28f94131bb6a1db7592a198cd79cf78ffda6bb9))
* use released version of hapi-nextauth ([96525a8](https://gitlab.com/digiresilience/link/amigo/commit/96525a8f1caf10bdafc2199521b31d3515930d69))

### [0.3.3](https://gitlab.com/digiresilience/link/amigo/compare/0.3.2...0.3.3) (2020-12-01)


### Bug Fixes

* nextauth adapter will work when user has no linked accounts ([7a47d98](https://gitlab.com/digiresilience/link/amigo/commit/7a47d98be689691332d17ded96a1efb71632fa05)), closes [/github.com/nextauthjs/next-auth/issues/876#issuecomment-732277061](https://gitlab.com/digiresilience//github.com/nextauthjs/next-auth/issues/876/issues/issuecomment-732277061)
* support the schmervice functional interface in typescript ([cf67d86](https://gitlab.com/digiresilience/link/amigo/commit/cf67d864fd909e51b139eef23b2c70f43514e362))

### [0.3.2](https://gitlab.com/digiresilience/link/amigo/compare/0.3.1...0.3.2) (2020-11-23)

### [0.3.1](https://gitlab.com/digiresilience/link/amigo/compare/0.3.0...0.3.1) (2020-11-23)


### Features

* ensure cookies and cloudflare auth headers are not logged ([91e26c3](https://gitlab.com/digiresilience/link/amigo/commit/91e26c3644249f81974abf75022f0b2163f5c7e0))

## [0.3.0](https://gitlab.com/digiresilience/link/amigo/compare/0.2.0...0.3.0) (2020-11-21)


### ⚠ BREAKING CHANGES

* Change default ports by + 1

### Features

* Change default ports by + 1 ([f3b4f07](https://gitlab.com/digiresilience/link/amigo/commit/f3b4f07a6086381c1957105cef47f20a9cb5fccb))

## [0.2.0](https://gitlab.com/digiresilience/link/amigo/compare/0.1.2...0.2.0) (2020-11-20)

### [0.1.2](https://gitlab.com/digiresilience/link/amigo/compare/0.1.1...0.1.2) (2020-11-12)


### Features

* add function to nicely print the config ([bf3f818](https://gitlab.com/digiresilience/link/amigo/commit/bf3f818c8ba8bb64d93f07ca9ed9329b14ec4d67))
* logger will ignore status and swagger endpoints ([979eb0a](https://gitlab.com/digiresilience/link/amigo/commit/979eb0ac6e351c78dedd22681c2118aab5fc1a9e))


### Bug Fixes

* bunch of little bug fixes in the crud repository ([886fac5](https://gitlab.com/digiresilience/link/amigo/commit/886fac5f425466d68b2f8d694fbe33717a48b0f9))
* shorten status endpoint docs ([080b3f5](https://gitlab.com/digiresilience/link/amigo/commit/080b3f50c22ea782e93893cca18cce14b85a8861))

### [0.1.1](https://gitlab.com/digiresilience/link/amigo/compare/0.1.0...0.1.1) (2020-11-11)


### Features

* Allow for config to be loaded from a file. ([37a090f](https://gitlab.com/digiresilience/link/amigo/commit/37a090f21b673170d947c894851b5c4610be8b5d))


### Bug Fixes

* open up the registerPlugin type ([ca543ab](https://gitlab.com/digiresilience/link/amigo/commit/ca543ab017bb0f01e45a09b3c282625cdc8fae0f))

## 0.1.0 (2020-11-11)
