import { Server } from "@hapi/hapi";
import cloneDeep from "lodash/cloneDeep";
import { deepFreeze } from "../helpers";

interface ConfigOptions {
  config: unknown;
}

const register = async (
  server: Server,
  options: ConfigOptions
): Promise<void> => {
  const safeConfig = deepFreeze(cloneDeep(options.config));
  server.decorate("server", "config", () => safeConfig);
};

const ConfigPlugin = {
  register,
  name: "config",
  version: "0.0.1",
};

export default ConfigPlugin;
