import pino, { LoggerOptions } from "pino";
import { IAmigoConfig } from "./config";

export const getPrettyPrint = <T extends IAmigoConfig>(config: T): boolean => {
  const { prettyPrint } = config.logging;
  if (prettyPrint === "auto") return config.isDev;
  return prettyPrint === true;
};

export const configureLogger = <T extends IAmigoConfig>(
  config: T
): pino.Logger => {
  const { level, redact } = config.logging;
  const options: LoggerOptions = {
    level,
    prettyPrint: getPrettyPrint(config),
    redact: {
      paths: redact,
      remove: true,
    },
  };
  return pino(options);
};
