import { ConvictSchema } from "./types";

export interface ICorsConfig {
  allowedMethods: Array<string>;
  allowedOrigins: Array<string>;
  allowedHeaders: Array<string>;
}

export const CorsConfig: ConvictSchema<ICorsConfig> = {
  allowedMethods: {
    doc: "The allowed CORS methods",
    format: "Array",
    env: "CORS_ALLOWED_METHODS",
    default: ["GET", "PUT", "POST", "PATCH", "DELETE", "HEAD", "OPTIONS"],
  },
  allowedOrigins: {
    doc: "The allowed origins",
    format: "Array",
    env: "CORS_ALLOWED_ORIGINS",
    default: [],
  },
  allowedHeaders: {
    doc: "The allowed headers",
    format: "Array",
    env: "CORS_ALLOWED_HEADERS",
    default: [
      "content-type",
      "authorization",
      "cf-access-authenticated-user-email",
    ],
  },
};
