import { ConvictSchema } from "./types";

export interface IServerConfig {
  address: string;
  port: number;
}

export const ServerConfig: ConvictSchema<IServerConfig> = {
  address: {
    doc: "The IP address to bind the server to",
    format: "ipaddress",
    default: "0.0.0.0",
    env: "SERVER_ADDRESS",
  },
  port: {
    doc: "The port to bind the server to",
    format: "port",
    default: 3001,
    env: "SERVER_PORT",
  },
};
