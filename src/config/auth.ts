import { ConvictSchema } from "./types";

export interface ISessionConfig {
  sessionMaxAgeSeconds: number;
  sessionUpdateAgeSeconds: number;
}

export const SessionConfig: ConvictSchema<ISessionConfig> = {
  sessionMaxAgeSeconds: {
    doc: "How long in seconds until an idle session expires and is no longer valid.",
    format: "positiveInt",
    default: 30 * 24 * 60 * 60, // 30 days
    env: "SESSION_MAX_AGE_SECONDS",
  },
  sessionUpdateAgeSeconds: {
    doc: `Throttle how frequently in seconds to write to database to extend a session.
          Use it to limit write operations. Set to 0 to always update the database.
          Note: This option is ignored if using JSON Web Tokens`,
    format: "positiveInt",
    default: 24 * 60 * 60, // 24 hours
    env: "SESSION_UPDATE_AGE_SECONDS",
  },
};
