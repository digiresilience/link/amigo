import convict from "convict";

/*
interface SSMObj {
  path: string;
}
 */

// eslint-disable-next-line @typescript-eslint/no-explicit-any
interface ConvictSchemaObj<T = any> extends convict.SchemaObj<T> {
  // ssm?: SSMObj;
  /**
   * The config item will be ignored for purposes of config file generation
   */
  skipGenerate?: boolean;
}

export type ConvictSchema<T> = {
  [P in keyof T]: convict.Schema<T[P]> | ConvictSchemaObj<T[P]>;
};

export interface ExtendedConvict<T> extends convict.Config<T> {
  isProd?: boolean;
  isTest?: boolean;
  isDev?: boolean;
}
