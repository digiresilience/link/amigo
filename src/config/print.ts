import chalk from "chalk";
import convict from "convict";

const visitLeaf = (path, key, leaf) => {
  if (leaf.skipGenerate) {
    return;
  }

  let name = `${path}.${key}`;
  if (path.length === 0) name = key;
  console.log(chalk.green(name));
  console.log(leaf.doc);
  if (leaf.default === undefined) {
    console.log(chalk.red("\t required"));
  } else {
    console.log(`\tdefault: ${JSON.stringify(leaf.default)}`);
  }

  console.log(`\tformat: ${leaf.format}`);
  console.log(`\tenv: ${leaf.env}`);
};

const visitNode = (path, node, key = "") => {
  if (node._cvtProperties) {
    const keys = Object.keys(node._cvtProperties);
    const subpath = key === "" ? path : `${key}`;

    keys.forEach((key) => {
      visitNode(subpath, node._cvtProperties[key], key);
    });
    console.log();
  } else {
    visitLeaf(path, key, node);
  }
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const printConfigOptions = (conf: convict.Config<any>): void => {
  const schema = conf.getSchema();
  visitNode("", schema);
};
