import { ConvictSchema } from "./types";

export interface IAppMetaConfig {
  name: string;
  version: string;
  figletFont: string;
}

export const AppMetaConfig: ConvictSchema<IAppMetaConfig> = {
  version: {
    doc: "The current application version",
    format: String,
    env: "npm_package_version",
    default: undefined,
    skipGenerate: true,
  },
  name: {
    doc: "Application name",
    format: String,
    env: "npm_package_name",
    default: undefined,
    skipGenerate: true,
  },
  figletFont: {
    doc: "The figlet font name used to print the site name on boot",
    format: String,
    env: "FIGLET_FONT",
    default: "Sub-Zero",
    skipGenerate: true,
  },
};
