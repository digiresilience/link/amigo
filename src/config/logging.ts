import { ConvictSchema } from "./types";

export interface ILoggingConfig {
  level: string;
  sql: boolean;
  redact: string[];
  ignorePaths: string[];
  ignoreTags: string[];
  requestIdHeader: string;
  logRequestStart: boolean;
  logRequestComplete: boolean;
  logRequestPayload: boolean;
  logRequestQueryParams: boolean;
  prettyPrint: boolean | "auto";
}

export const LoggingConfig: ConvictSchema<ILoggingConfig> = {
  level: {
    doc: "The logging level",
    format: ["trace", "debug", "info", "warn", "error"],
    default: "info",
    env: "LOG_LEVEL",
  },
  sql: {
    doc: "Whether to log sql statements",
    format: "Boolean",
    default: false,
    env: "LOG_SQL",
  },
  redact: {
    doc: "Pino redaction array. These are always redacted. see https://getpino.io/#/docs/redaction",
    format: "Array",
    default: [
      "req.remoteAddress",
      "req.headers.authorization",
      `req.headers["cf-access-jwt-assertion"]`,
      `req.headers["cf-access-authenticated-user-email"]`,
      `req.headers["cf-connecting-ip"]`,
      `req.headers["cf-ipcountry"]`,
      `req.headers["x-forwarded-for"]`,
      "req.headers.cookie",
    ],
  },
  ignorePaths: {
    doc: "Ignore http paths (exact) when logging requests",
    format: "Array",
    default: ["/graphql"],
  },
  ignoreTags: {
    doc: "Ignore routes tagged with these tags when logging requests",
    format: "Array",
    default: ["status", "swagger", "nolog"],
  },
  requestIdHeader: {
    doc: "The header where the request id lives",
    format: String,
    default: "x-request-id",
    env: "REQUEST_ID_HEADER",
  },
  logRequestStart: {
    doc: "Whether hapi-pino should add a log.info() at the beginning of Hapi requests for the given Request.",
    format: "Boolean",
    default: false,
    env: "LOG_REQUEST_START",
  },
  logRequestComplete: {
    doc: "Whether hapi-pino should add a log.info() at the completion of Hapi requests for the given Request.",
    format: "Boolean",
    default: true,
    env: "LOG_REQUEST_COMPLETE",
  },
  logRequestPayload: {
    doc: "When enabled, add the request payload as payload to the response event log.",
    format: "Boolean",
    default: false,
    env: "LOG_REQUEST_PAYLOAD",
  },
  logRequestQueryParams: {
    doc: "When enabled, add the request query as queryParams to the response event log.",
    format: "Boolean",
    default: false,
    env: "LOG_REQUEST_QUERY_PARAMS",
  },
  prettyPrint: {
    doc: "Pretty print the logs",
    format: ["auto", true, false],
    default: "auto",
    env: "LOG_PRETTY_PRINT",
  },
};
