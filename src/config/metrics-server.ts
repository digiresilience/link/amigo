import { ConvictSchema } from "./types";

export interface IMetricsConfig {
  address: string;
  port: number;
}

export const MetricsConfig: ConvictSchema<IMetricsConfig> = {
  address: {
    doc: "The ip address to bind the prometheus metrics to",
    format: "ipaddress",
    default: "127.0.0.1",
    env: "METRICS_ADDRESS",
  },
  port: {
    doc: "The port to bind the prometheus metrics to",
    format: "port",
    default: 3002,
    env: "METRICS_PORT",
    arg: "port",
  },
};
