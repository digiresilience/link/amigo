import { recordInfo } from "./record-info";
import { RepositoryBase } from "./base";
import { Flavor, UUID } from "../helpers";
import { UserId } from "./user";

export type SessionId = Flavor<UUID, "Session Id">;

export interface UnsavedSession {
  userId: UserId;
  expires: Date;
  sessionToken: string;
  accessToken: string;
}

export interface SavedSession extends UnsavedSession {
  id: SessionId;
  createdAt: Date;
  updatedAt: Date;
}

export const SessionRecord = recordInfo<UnsavedSession, SavedSession>(
  "app_private",
  "sessions"
);

export class SessionRecordRepository extends RepositoryBase(SessionRecord) {}
