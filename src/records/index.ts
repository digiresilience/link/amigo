export * from "./base";
export * from "./record-info";
export * from "./crud-repository";
export * from "./user";
export * from "./session";
export * from "./account";

import type { AccountRecordRepository } from "./account";
import type { UserRecordRepository } from "./user";
import type { SessionRecordRepository } from "./session";

export interface IAmigoRepositories {
  users: UserRecordRepository;
  sessions: SessionRecordRepository;
  accounts: AccountRecordRepository;
}
