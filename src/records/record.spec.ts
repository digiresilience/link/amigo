import * as Joi from "joi";
import { unboundRepositoryBase } from "./base";
import type { Flavor, UUID } from "../helpers";
import type { IAmigoRepositories } from "../records";

import { startPgp, stopPgp, startDb, stopDb } from "../helpers.test";
import {
  PgRecordInfo,
  recordInfo,
  collectIdValues,
  compositeRecordType,
} from "./record-info";
const createTable = async (db) => {
  await db.none(`CREATE TABLE public.users
(id uuid not null default gen_random_uuid() primary key,
name text,
email text
)`);

  await db.none(`CREATE TABLE public.composite
(id1 uuid not null default gen_random_uuid(),
id2 uuid not null default gen_random_uuid(),
name text,
PRIMARY KEY (id1, id2))`);

  await db.none(
    "INSERT INTO public.users (name, email) VALUES('Tester', 'tester@tester.tester')"
  );
  await db.none("INSERT INTO public.composite (name) VALUES('Tester')");
};

const dropTable = async (db) => {
  await db.none(`DROP TABLE IF EXISTS public.users, public.composite CASCADE`);
};

type UserId = Flavor<UUID, "User Id">;

interface UnsavedUser {
  name: string;
  email: string;
}

interface SavedUser extends UnsavedUser {
  id: UserId;
}

const UserRecord = recordInfo<UnsavedUser, SavedUser>("public", "users");

function RepositoryBase<Rec extends PgRecordInfo>(recordType: Rec) {
  return unboundRepositoryBase<Rec, IAmigoRepositories>(recordType);
}

class UserRecordRepository extends RepositoryBase(UserRecord) {}

type CompositeId = Flavor<UUID, "Composite Id">;

interface UnsavedComposite {
  name: string;
}

interface SavedComposite extends UnsavedComposite {
  id1: CompositeId;
  id2: CompositeId;
}

const CompositeRecord = compositeRecordType<UnsavedComposite, SavedComposite>(
  "public",
  "composite"
).withCompositeKeys(["id1", "id2"]);

class CompositeRecordRepository extends RepositoryBase(CompositeRecord) {}

const toCreate = {
  name: "Tester1",
};

const toCreate2 = {
  name: "Tester2",
};
describe("record with single valued primary key", () => {
  let pgp;
  let db;
  let repo;
  beforeEach(async () => {
    pgp = await startPgp();
    db = await startDb(pgp);
    await dropTable(db);
    await createTable(db);
    repo = new UserRecordRepository(db);
  });
  afterEach(async () => {
    await dropTable(db);
    await stopDb(db);
    await stopPgp(pgp);
  });

  it("count", async () => {
    expect(await repo.count()).toBe(1);
  });
  it("countBy", async () => {
    expect(await repo.countBy({ name: "Tester" })).toBe(1);
  });
  it("findAll", async () => {
    const result = await repo.findAll();
    expect(result[0].email).toBe("tester@tester.tester");
    expect(result).toHaveLength(1);
    expect(Joi.string().uuid().validate(result[0].id)).toBeTruthy();
  });
  it("findBy", async () => {
    const tester = await repo.findBy({
      email: "tester@tester.tester",
    });
    expect(tester.email).toBe("tester@tester.tester");
    expect(tester.name).toBe("Tester");
  });

  it("findById", async () => {
    const tester: SavedUser = await repo.findBy({
      email: "tester@tester.tester",
    });
    const result = await repo.findById(tester.id);
    expect(result.email).toBe("tester@tester.tester");
    expect(result.name).toBe("Tester");
  });

  test("existsById", async () => {
    const { id } = await repo.findBy({ email: "tester@tester.tester" });
    expect(await repo.existsById(id)).toBe(true);
  });

  it("findAllBy", async () => {
    const result = await repo.findAllBy({
      email: "tester@tester.tester",
    });
    expect(result[0].email).toBe("tester@tester.tester");
    expect(result[0].name).toBe("Tester");
    expect(result).toHaveLength(1);
  });

  test("insert", async () => {
    const created = await repo.insert(toCreate);
    expect(created).toMatchObject(toCreate);
  });

  test("insertAll and removeAll", async () => {
    const created = await repo.insertAll([toCreate, toCreate2]);
    expect(created).toHaveLength(2);
    expect(await repo.count()).toBe(3);
    await repo.removeAll(created);
    expect(await repo.count()).toBe(1);
    await repo.removeAll();
    expect(await repo.count()).toBe(0);
  });

  test("updateById", async () => {
    const { id, email, name } = await repo.findBy({
      email: "tester@tester.tester",
    });
    expect(name).toBe("Tester");
    const { name: name2, email: email2 } = await repo.updateById(id, {
      name: "Tester (edited)",
    });
    expect(email2).toBe(email);
    expect(name2).toBe("Tester (edited)");
  });

  test("update", async () => {
    const tester = await repo.findBy({
      email: "tester@tester.tester",
    });
    expect(tester.name).toBe("Tester");
    const edited = {
      ...tester,
      name: "Tester (edited)",
    };
    const { name: name2, email: email2 } = await repo.update(edited);
    expect(email2).toBe(tester.email);
    expect(name2).toBe("Tester (edited)");
  });

  test("updateAll", async () => {
    const created = await repo.insertAll([toCreate, toCreate2]);

    expect(created[0].name).toBe("Tester1");
    expect(created[1].name).toBe("Tester2");

    created[0].name = "Edited1";
    created[1].name = "Edited2";

    const result = await repo.updateAll(created);
    expect(result[0].name).toBe("Edited1");
    expect(result[1].name).toBe("Edited2");
  });

  test("removeBy", async () => {
    expect(await repo.count()).toBe(1);
    await repo.removeBy({ name: "Tester" });
    expect(await repo.count()).toBe(0);
  });
});

describe("record with composite valued primary key", () => {
  let pgp;
  let db;
  let repo;

  beforeEach(async () => {
    pgp = await startPgp();
    db = await startDb(pgp);
    await dropTable(db);
    await createTable(db);
    repo = new CompositeRecordRepository(db);
  });
  afterEach(async () => {
    await dropTable(db);
    await stopDb(db);
    await stopPgp(pgp);
  });

  it("count", async () => {
    expect(await repo.count()).toBe(1);
  });

  it("countBy", async () => {
    expect(await repo.countBy({ name: "Tester" })).toBe(1);
  });

  it("findAll", async () => {
    const result = await repo.findAll();
    expect(result[0].name).toBe("Tester");
    expect(result).toHaveLength(1);
    expect(Joi.string().uuid().validate(result[0].id1)).toBeTruthy();
    expect(Joi.string().uuid().validate(result[0].id2)).toBeTruthy();
  });

  it("findBy", async () => {
    const tester = await repo.findBy({
      name: "Tester",
    });
    expect(tester.name).toBe("Tester");
  });

  it("findById - throws when passed empty", async () => {
    expect(repo.findById({})).rejects.toThrow();
  });

  it("findById - object", async () => {
    const tester = await repo.findBy({
      name: "Tester",
    });
    const ids = CompositeRecord.idOf(tester);
    const result = await repo.findById(ids);
    expect(result.name).toBe("Tester");
  });

  it("findById - array", async () => {
    const tester = await repo.findBy({
      name: "Tester",
    });
    const ids = collectIdValues(tester, CompositeRecord);
    const result = await repo.findById(ids);
    expect(result.name).toBe("Tester");
  });

  test("existsById", async () => {
    const tester = await repo.findBy({ name: "Tester" });
    const ids = collectIdValues(tester, CompositeRecord);
    expect(await repo.existsById(ids)).toBe(true);
  });

  it("findAllBy", async () => {
    const result = await repo.findAllBy({
      name: "Tester",
    });
    expect(result[0].name).toBe("Tester");
    expect(result).toHaveLength(1);
  });

  test("insert", async () => {
    const created = await repo.insert(toCreate);
    expect(created).toMatchObject(toCreate);
  });

  test("insertAll and removeAll", async () => {
    const created = await repo.insertAll([toCreate, toCreate2]);
    expect(created).toHaveLength(2);
    expect(await repo.count()).toBe(3);
    await repo.removeAll(created);
    expect(await repo.count()).toBe(1);
    await repo.removeAll();
    expect(await repo.count()).toBe(0);
  });

  test("updateById", async () => {
    const tester = await repo.findBy({
      name: "Tester",
    });
    expect(tester.name).toBe("Tester");
    const ids = CompositeRecord.idOf(tester);
    const { name: name2 } = await repo.updateById(ids, {
      name: "Tester (edited)",
    });
    expect(name2).toBe("Tester (edited)");
  });

  test("update", async () => {
    const tester = await repo.findBy({
      name: "Tester",
    });
    expect(tester.name).toBe("Tester");
    const edited = { ...tester, name: "Tester (edited)" };
    const { name: name2 } = await repo.update(edited);
    expect(name2).toBe("Tester (edited)");
  });

  test("updateAll", async () => {
    const created = await repo.insertAll([toCreate, toCreate2]);

    expect(created[0].name).toBe("Tester1");
    expect(created[1].name).toBe("Tester2");

    created[0].name = "Edited1";
    created[1].name = "Edited2";

    const result = await repo.updateAll(created);
    expect(result[0].name).toBe("Edited1");
    expect(result[1].name).toBe("Edited2");
  });
});

process.on("unhandledRejection", (err) => {
  throw err;
});
