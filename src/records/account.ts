import { recordInfo } from "./record-info";
import { RepositoryBase } from "./base";
import { Flavor, UUID } from "../helpers";
import { UserId } from "./user";

export type AccountId = Flavor<UUID, "Account Id">;

export interface UnsavedAccount {
  compoundId: string;
  userId: UserId;
  providerType: string;
  providerId: string;
  providerAccountId: string;
  refreshToken: string;
  accessToken: string;
  accessTokenExpires: Date;
}

export interface SavedAccount extends UnsavedAccount {
  id: AccountId;
  createdAt: Date;
  updatedAt: Date;
}

export const AccountRecord = recordInfo<UnsavedAccount, SavedAccount>(
  "app_public",
  "accounts"
);

export class AccountRecordRepository extends RepositoryBase(AccountRecord) {}
