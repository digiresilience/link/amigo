/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import process from "process";
import camelcaseKeys from "camelcase-keys";
import * as pgMonitor from "pg-monitor";
import pgPromise, { IMain } from "pg-promise";

export const startPgp = async () => {
  const opts = {
    noWarnings: true,
    receive(data, result) {
      result.rows = camelcaseKeys(data);
    },
  };
  const pgp: IMain = pgPromise(opts);
  if (process.env.LOG_SQL) pgMonitor.attach(opts);
  return pgp;
};

export const stopPgp = async (pgp) => {
  if (process.env.LOG_SQL) await pgMonitor.detach();
  return pgp.end();
};

export const startDb = async (pgp) => {
  // Creating the database instance with extensions:
  let db = pgp("postgresql://amigo:amigo@127.0.0.1:5436/amigo_test");
  if (process.env.CI)
    db = pgp("postgresql://amigo:amigo@postgres:5432/amigo_test");

  return db;
};

export const stopDb = async (db) => db.$pool.end();
