import * as PGP from "pg-promise";
import * as PGPTS from "pg-promise/typescript/pg-subset";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type IDatabase = PGP.IDatabase<any>;
export type IMain = PGP.IMain;
export type IResult = PGPTS.IResult;
export type IInitOptions = PGP.IInitOptions;
