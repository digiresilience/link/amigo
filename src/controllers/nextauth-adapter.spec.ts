import { startPgp, stopPgp, startDb, stopDb } from "../helpers.test";
import { AccountRecordRepository } from "../records/account";
import { UserRecordRepository } from "../records/user";
import { SessionRecordRepository } from "../records/session";

import { NextAuthAdapter, defaultSessionMaxAge } from "./nextauth-adapter";
import { IAmigoRepositories } from "src/records";

const createTable = async (db) => {
  await db.none(`CREATE SCHEMA app_public`);
  await db.none(`CREATE SCHEMA app_private`);
  await db.none(`
CREATE TABLE app_public.users
  (
    id             SERIAL,
    name           VARCHAR(255),
    email          VARCHAR(255),
    email_verified TIMESTAMPTZ,
    avatar          VARCHAR(255),
    user_role       VARCHAR(255),
    is_active      boolean               not null default false,
    created_at     TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at     TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    constraint     users_email_unique    unique (email),
    PRIMARY KEY (id)
  )`);

  await db.none(`CREATE TABLE app_private.sessions
  (
    id            SERIAL,
    user_id       INTEGER NOT NULL,
    expires       TIMESTAMPTZ NOT NULL,
    session_token VARCHAR(255) NOT NULL,
    access_token  VARCHAR(255) NOT NULL,
    created_at    TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at    TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);`);

  await db.none(`
CREATE TABLE app_public.accounts
  (
    id                   SERIAL,
    compound_id          VARCHAR(255) NOT NULL,
    user_id              INTEGER NOT NULL,
    provider_type        VARCHAR(255) NOT NULL,
    provider_id          VARCHAR(255) NOT NULL,
    provider_account_id  VARCHAR(255) NOT NULL,
    refresh_token        TEXT,
    access_token         TEXT,
    access_token_expires TIMESTAMPTZ,
    created_at           TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at           TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);`);
};

const dropTable = async (db) =>
  db.none(`DROP SCHEMA IF EXISTS app_public, app_private CASCADE`);

describe("nextauth adapter", () => {
  let pgp;
  let db;
  let repos: IAmigoRepositories;
  let adapter;
  beforeEach(async () => {
    pgp = await startPgp();
    db = await startDb(pgp);
    await dropTable(db);
    await createTable(db);

    repos = {
      users: new UserRecordRepository(db),
      accounts: new AccountRecordRepository(db),
      sessions: new SessionRecordRepository(db),
    };
    adapter = new NextAuthAdapter(repos);
  });

  afterEach(async () => {
    await dropTable(db);
    await stopDb(db);
    await stopPgp(pgp);
  });

  const createAlice = async () =>
    adapter.createUser({
      name: "Alice",
      email: "a@a.a",
      avatar: "",
      emailVerified: new Date(),
    });

  it("createUser", async () => {
    const user = await createAlice();
    expect(user.name).toBe("Alice");
    expect(user.isActive).toBe(false);
  });

  it("getUser - no accounts", async () => {
    const alice = await createAlice();
    const user = await adapter.getUser(alice.id);
    // adapter returns null when a user has no linked accounts
    expect(user).toBeNull();
  });

  it("getUserByEmail - no accounts", async () => {
    const alice = await createAlice();
    const user = await adapter.getUserByEmail(alice.email);
    // adapter returns null when a user has no linked accounts
    expect(user).toBeNull();
  });

  it("linkAccount and getUser", async () => {
    const alice = await createAlice();
    const expires = Date.now();
    await adapter.linkAccount(
      alice.id,
      "providerA",
      "providerType",
      "alice",
      "access",
      "refresh",
      expires
    );
    const account = await adapter.repos.accounts.findBy({ userId: alice.id });
    expect(account.providerId).toBe("providerA");
    expect(account.accessTokenExpires).toStrictEqual(new Date(expires));

    const user1 = await adapter.getUser(alice.id);
    expect(user1.name).toBe("Alice");
    const user2 = await adapter.getUserByEmail(alice.email);
    expect(user2.name).toBe("Alice");
  });

  it("getUserByProviderAccountId", async () => {
    const alice = await createAlice();
    const expires = Date.now();
    await adapter.linkAccount(
      alice.id,
      "providerA",
      "providerType",
      "alice",
      "access",
      "refresh",
      expires
    );
    const user = await adapter.getUserByProviderAccountId("providerA", "alice");
    expect(user).toStrictEqual(alice);
  });

  it("updateUser", async () => {
    const alice = await createAlice();

    const user = await adapter.updateUser({ ...alice, email: "b@b.b" });
    expect(user.name).toBe("Alice");
    expect(user.email).toBe("b@b.b");
  });

  it("unLinkAccount", async () => {
    const alice = await createAlice();
    const expires = Date.now();
    await adapter.linkAccount(
      alice.id,
      "providerA",
      "providerType",
      "alice",
      "access",
      "refresh",
      expires
    );
    const account = await adapter.repos.accounts.findBy({ userId: alice.id });
    expect(account.providerId).toBe("providerA");
    await adapter.unlinkAccount(alice.id, "providerA", "alice");
    const account2 = await adapter.repos.accounts.findBy({ userId: alice.id });
    expect(account2).toBeNull();
  });

  it("createSession", async () => {
    const user = await createAlice();
    const creationTime = Date.now();
    const session = await adapter.createSession(user);
    expect(session.userId).toBe(user.id);
    expect(session.sessionToken).toHaveLength(64);
    expect(session.accessToken).toHaveLength(64);
    expect(session.expires.getTime()).toBeGreaterThanOrEqual(
      creationTime + defaultSessionMaxAge
    );
  });

  it("getSession", async () => {
    const alice = await createAlice();
    const session = await adapter.createSession(alice);
    const got = await adapter.getSession(session.sessionToken);
    expect(got).toStrictEqual(session);
  });

  it("getSession - expired session returns null", async () => {
    const alice = await createAlice();
    const session = await adapter.createSession(alice);
    await adapter.repos.sessions.update({ ...session, expires: new Date(1) });
    const got = await adapter.getSession(session.sessionToken);
    expect(got).toBeNull();
  });

  it("updateSession - old session is updated", async () => {
    const alice = await createAlice();
    const session = await adapter.createSession(alice);
    // munge the date to be in the past
    const mungedSession = await adapter.repos.sessions.update({
      ...session,
      expires: new Date(Date.now() - 24 * 60 * 60 * 1000),
    });
    const session2 = await adapter.updateSession(mungedSession);
    expect(session2.expires.getTime()).toBeGreaterThan(
      session.expires.getTime()
    );
  });

  it("updateSession - fresh session is not updated", async () => {
    const alice = await createAlice();
    const session = await adapter.createSession(alice);
    // munge the date to be in the past
    const mungedSession = await adapter.repos.sessions.update({
      ...session,
      expires: new Date(session.expires.getTime() + 24 * 60 * 60 * 1000),
    });
    const session2 = await adapter.updateSession(mungedSession);
    expect(session2).toBeNull();
  });

  it("updateSession - break sessionMaxAge", async () => {
    const alice = await createAlice();
    const session = await adapter.createSession(alice);
    delete adapter.sessionMaxAge;
    const session2 = await adapter.updateSession(session);
    expect(session2).toBeNull();
  });

  it("updateSession - break update age", async () => {
    const alice = await createAlice();
    const session = await adapter.createSession(alice);
    delete adapter.sessionUpdateAge;
    const session2 = await adapter.updateSession(session);
    expect(session2).toBeNull();
  });

  it("updateSession - force when broken", async () => {
    const alice = await createAlice();
    const session = await adapter.createSession(alice);
    delete adapter.sessionUpdateAge;
    const session2 = await adapter.updateSession(session, true);
    expect(session2.expires.getTime()).toBeGreaterThanOrEqual(
      session.expires.getTime()
    );
  });

  it("deleteSession", async () => {
    const alice = await createAlice();
    const session = await adapter.createSession(alice);
    await adapter.deleteSession(session.sessionToken);
    const got = await adapter.getSession(session.sessionToken);
    expect(got).toBeNull();
  });
});
