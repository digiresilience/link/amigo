export * from "./config";
export * from "./controllers/crud-controller";
export * from "./controllers/nextauth-adapter";
export * from "./hapi";
export * from "./helpers";
export * from "./helpers/response";
export * from "./helpers/validation-error";
export * from "./logger";
export * from "./records";

import * as pino from "pino";

declare module "@hapi/hapi" {
  interface Server {
    logger: pino.Logger;
  }

  interface Request {
    logger: pino.Logger;
  }
}
